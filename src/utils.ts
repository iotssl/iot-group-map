import * as geojson from 'geojson';


export const mobileAndTabletcheck = (): boolean => {
    const isMobileDevice = window.navigator.userAgent.toLowerCase().includes("mobi");
    return isMobileDevice;
};

export class GeoJson {
    constructor() {

    }

    mergeGeoJson(data: (geojson.FeatureCollection<geojson.GeometryObject> | geojson.Feature<geojson.GeometryObject>)[]): geojson.FeatureCollection<geojson.GeometryObject> {
        let features = data.map((data, i, a) => {
            switch (data.type) {
                case "FeatureCollection":
                  return data.features;
              
                case "Feature":
                  return [data];
              
                default:
                  console.warn("Unrecognized GeoJSON:", data);
                }
        }, this).reduce((a, b) => {
            return a.concat(b);
          });

        var seen = [];
        
        features = features.filter((x): boolean => {
            if (x.id) {
                if (seen.indexOf(x.id) >= 0) {
                return false;
                }
            
                seen.push(x.id);
            }
            
            return true;
            });

        let result: geojson.FeatureCollection<geojson.GeometryObject>
        result.features = features
        return result
    }
}