<sidebar>
  <div id="sidebar" class="sidebar collapsed">
    <!-- Nav tabs -->
    <div class="sidebar-tabs">
        <ul role="tablist">
            <li><a href="#home" role="tab"><i class="fa fa-bars"></i></a></li>
            <li><a href="#info" role="tab"><i class="fa fa-user"></i></a></li>
        </ul>

        <ul role="tablist">
            <li><a href="#settings" role="tab"><i class="fa fa-gear"></i></a></li>
        </ul>
    </div>

    <!-- Tab panes -->
    <div class="sidebar-content">
        <div class="sidebar-pane" id="home">
            <h1 class="sidebar-header">
                Iot Map
                <span class="sidebar-close"><i class="fa fa-caret-left"></i></span>
            </h1>

            <p>Die Iot-Map ist ein Projekt des <a target="_blank" rel="noopener" href="https://gitlab.com/iotssl/" title="IoT Südschleswig">IoT Südschleswig</a> für die <a target="_blank" rel="noopener" href="https://iot-usergroup.de/" title="IoT-Usergroup Deutschland">IoT-Usergroup Deutschland</a>.</p>

            <p>Ziel der Map ist eine Map mit Orten, welche zeigen wo Iot Gruppen sich befinden und wie diese kontaktiert werden können.</p>

        </div>

        <div class="sidebar-pane" id="info">
            <h1 class="sidebar-header">Community Info<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>

            <h2 id="community">Example Community</h2>
        </div>

        <div class="sidebar-pane" id="settings">
            <h1 class="sidebar-header">Settings<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
        </div>
    </div>
  </div>
</sidebar>