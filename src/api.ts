import * as request from 'browser-request'
import * as Ajv from 'ajv';
import * as geojson from 'geojson';

import Promise = require('bluebird')

export default class Api {
  private schema: object
  private apiIndex: object[]
  private ajv: Ajv.Ajv

  constructor() {
  
  }

  getCommunities(): Array<string> {
    let result: Array<string> = []

    this.apiIndex.forEach((v,i,a)=>{
      result.push(v["community"])
    })
    return result
  }

  getGeoJSON(community: string): geojson.FeatureCollection<geojson.GeometryObject> {
    let geoJson: geojson.FeatureCollection<geojson.GeometryObject>

    this.apiIndex.forEach((v,i,n) => {
      if (v["community"] == community) {
        geoJson = v["data"]["geoJSONFeatures"]
      }
    }, this)
    return geoJson
  }

  //private processData(): Promise<boolean> {

  //}

  loadData(): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      //Load Index File
      this.getApiIndexFile().then((data: object[]) => {

        //Load each API File
        let apis = 0
        data.forEach((item, index, array) => {

          //Download each API File
          this.getApiFile(index).then(data => {
            //Check each API File for validity and if wrong remove it
            this.validateApiFile(data).catch(() => {
              apis++
              console.log("invalid API file!")
              this.apiIndex[index]["data"] = null

              if (index === array.length-1 && index === apis-1) {
                resolve(true)
              }
            }).then(() => {
              apis++
              
              if (index === array.length-1 && index === apis-1) {
                resolve(true)
              }
            })
          })
        })
      })
    })
  }

  private getSchema(): Promise<object> {
    return new Promise<object>((resolve, reject) => {
      if (!this.schema) {
        request(window.location.origin + "/api/schema.json", (error: any, response: any, body: any) => {
          if (error) {
            reject(error)
          }
          this.schema = JSON.parse(body)
          resolve(this.schema)
        })
      } else {
        resolve(this.schema)
      }
    })
  }

  private getApiIndexFile(): Promise<object[]> {
    return new Promise<object[]>((resolve, reject) => {
        request(window.location.origin + "/api/directory.json", (error: any, response: any, body: any) => {
          if (error) {
            reject(error)
          }
        
          const api = JSON.parse(body)
          this.apiIndex = new Array<object>()
          api.forEach(element => {
            this.apiIndex.push(element)
          });
          resolve(this.apiIndex)
        })
    })
  }

  private getApiFile(community: number): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      if (!this.apiIndex[community]["data"]) {
        request(this.apiIndex[community]["url"], (error: any, response: any, body: any) => {
          if (error) {
            reject(error)
          }
          this.apiIndex[community]["data"] = JSON.parse(body)
          resolve(body)
        })
      } else {
        resolve(this.apiIndex[community]["data"])
      }
    })
  }

  private validateApiFile(data: string): Promise<boolean> {
    this.ajv = new Ajv({
      allErrors: true
    })

    return new Promise<boolean>((resolve, reject) => {
      this.getSchema().then(schema => {
        const validate = this.ajv.compile(schema);
        const valid = validate(JSON.parse(data))
    
        if (!valid) {
          console.error(validate.errors)
          reject(false)
        }
        resolve(true)
      })
            
    })
    
  }
}