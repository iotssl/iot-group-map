import * as riot from 'riot';
import './tags/app.tag';
import './index.css';
import Map from './map'

export default class Main {
    constructor() {
        riot.mount('*');
        const map = new Map('map')
        map.setLayerLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors')
    }
}

let start = new Main();