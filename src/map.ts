///<reference path="typings/leaflet-sidebar.d.ts" />
//import "leaflet/dist/leaflet.css"
import  * as L from 'leaflet';

import "sidebar-v2/css/leaflet-sidebar.min.css"
import 'sidebar-v2/js/leaflet-sidebar'
import * as geojson from 'geojson';

import * as request from 'browser-request'

import api from './api'

import {mobileAndTabletcheck} from './utils'

import Promise = require('bluebird')

export default class Map {
  private map: L.Map;
  private sidebar: L.Control.Sidebar;
  private apiData: api
  constructor(element: string) {

    this.map = new L.Map("map", {
        center: new L.LatLng(54.276656, 9.668946),
        zoom: 9,
        zoomControl: false
    })

    new L.Control.Zoom({
       position: 'topright'
    }).addTo(this.map)

    this.sidebar = new L.Control.Sidebar('sidebar')
    this.sidebar.addTo(this.map)
    if (!mobileAndTabletcheck()) {
      this.sidebar.open("home")
    }

    this.loadData()
    
  }

  setLayerLayer(url: string, attribution: string) {
    L.tileLayer(url, {
        attribution: attribution
      }).addTo(this.map);
  }

  private loadData() {
    this.api().loadData().then(() => {
      const communities = this.api().getCommunities()
      
          communities.forEach((v,i,a) => {
            const data = this.apiData.getGeoJSON(v)
            // Add Map Data to Map
            new L.GeoJSON(data, {
              onEachFeature: this.onCommunityAdd.bind(this),
              // Customize pointer
              pointToLayer: (feature, latlng): L.Layer => {
                const marker = new L.Marker(latlng, {
                  title: v || "Community Name nicht gefunden",
                  alt: v || "Community Name nicht gefunden"
                })
      
                return marker
              }
            }).addTo(this.map)
          })
    })
  }

  private onCommunityAdd(feature: geojson.Feature<geojson.GeometryObject>, layer: L.GeoJSON) {
    // Show Community Info on click

    switch (feature.geometry.type) {
      case "MultiPolygon":
        var marker = new L.Marker(layer.getBounds().getCenter(), {
          title: feature.properties.name || "Community Name nicht gefunden",
          alt: feature.properties.name || "Community Name nicht gefunden"
        }).on('click', function(event) {
          this.onCommunityClick(feature, layer.getBounds())
        }, this)
        .addTo(this.map);
    
        layer.on('click', function(event) {
          this.onCommunityClick(feature, layer.getBounds())
        }, this)
      case "Point":
       layer.on('click', function(event) {
          this.onCommunityClick(feature)
      }, this)
    }
  }

  private onCommunityClick(feature, bounds?) {
    if (bounds) {
      this.map.fitBounds(bounds);
    }


    // Get Community Name and fill the info box
    let CommunityName: string = feature.properties.name || "Community Name nicht gefunden"
    document.querySelector("h2#community").innerHTML = CommunityName
    
    //Open Info Page in Sidebar
    this.sidebar.open("info")
  }

  private api(): api {
    if (!this.apiData) {
      this.apiData = new api
    }
    return this.apiData
  }
}