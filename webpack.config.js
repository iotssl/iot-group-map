const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const nodeEnv = process.env.NODE_ENV || "development";
const isProd = nodeEnv === "production";
var CopyWebpackPlugin = require('copy-webpack-plugin');

var config = {
    devtool: isProd ? "hidden-source-map" : "source-map",
    context: path.resolve("./src"),
    entry: {
        index: "./index.ts",
        vendor: "./vendor.ts",
    },
    output: {
        path: path.resolve("./dist"),
        filename: "[name].bundle.js",
        sourceMapFilename: "[name].bundle.map",
        devtoolModuleFilenameTemplate: function(info) {
            return "file:///" + info.absoluteResourcePath;
        }
    },
    module: {
        rules: [{
                enforce: "pre",
                test: /\.tag$/,
                exclude: ["node_modules"],
                loader: 'riot-tag-loader',
                query: {
                    hot: true,
                    type: 'typescript'
                },
            },
            {
                enforce: "pre",
                test: /\.ts?$/,
                exclude: ["node_modules"],
                use: ["awesome-typescript-loader", "source-map-loader"]
            },
            {
                test: /\.html$ /,
                loader: "html-loader"
            },
            {
                test: /\.geojson$/,
                loader: 'json-loader'
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                    emitFile: true,
                }
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: 'url-loader'
            },
            { test: /\.css$/, loaders: ["style-loader", "css-loader"] }
        ]
    },
    resolve: {
        extensions: [".tag", ".ts", ".js", ".css"],
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                // eslint-disable-line quote-props
                NODE_ENV: JSON.stringify(nodeEnv)
            }
        }),
        new HtmlWebpackPlugin({
            title: "IoT Map",
            template: "!!ejs-loader!src/index.html"
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            minChunks: Infinity,
            filename: "vendor.bundle.js"
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            output: { comments: false },
            sourceMap: true
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                tslint: {
                    emitErrors: true,
                    failOnHint: true
                }
            }
        }),
        new CopyWebpackPlugin([
            // {output}/to/file.txt
            { from: '../datenGeoJson', to: 'data' },
            { from: '../apiFiles', to: 'api' },
        ], {

            // By default, we only copy modified files during
            // a watch or webpack-dev-server build. Setting this
            // to `true` copies all files.
            copyUnmodified: false
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist/"),
        compress: true,
        port: 3000,
        hot: true,
        headers: {
            "X-Access-Control-Allow-Origin": "*"
        },
        noInfo: true,
        watchContentBase: true
    }
};

module.exports = config;